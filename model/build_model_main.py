import logging

from itemized_billing.utility import definitions as defn
from itemized_billing.utility import doc_data
from itemized_billing.zippylog import zippylog
from model import page_classifier


def main():

    # configure logger
    zippylog.run(config_path_filename=defn.BASE_DIR + "/" + defn.LOG_CONFIG_DIR + "/" + defn.CONFIG_FILENAME,
                 log_dir=defn.LOG_DIR,
                 console_level=defn.CONSOLE_LEVEL,
                 env_key='LOG_CFG')

    # TODO add logging statements
    #logger = logging.getLogger(__name__)

    ddo = doc_data.DocumentData()

    pcdo = page_classifier.PageClassifierData(ddo, defn.SOURCE_TRAIN_CONST)

    # get derived data to build model with
    pcdo.calculate_derived_data()

    # build model
    pcdo.train_model()

    # output performance metrics
    pcdo.get_performance()

    # save model
    pcdo.save_model(keep=3)


if __name__ == "__main__":
    main()
