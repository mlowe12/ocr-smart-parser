import io
import logging
import sys

import cv2
import numpy as np
import pandas as pd
import pytesseract

from itemized_billing.utility import definitions as defn


class OCRImage(object):
    def __init__(self, image_filepointer, preprocess_pipe, splice_flag=False):
        # images
        self.splice_flag = splice_flag
        if self.splice_flag is False:
            # filepointer is a physical file
            self.og_image = cv2.imread(image_filepointer)
        else:
            # filepointer is a PIL.Image object
            print("HELP ME: ", type(image_filepointer))
            # image_filepointer.save('/tmp/debug.png')
            self.og_image = cv2.cvtColor(np.array(image_filepointer), cv2.COLOR_RGB2GRAY)
        self.transformed_image = None
        self.transformed_image_loc = None

        # pre-process pipe
        self.preprocess_pipe = preprocess_pipe

        # results
        self.ocr_result_df = None
        self.ocr_text_ls = None
        self.ocr_text_str = None
        self.total_row_count = None

        self.logger = logging.getLogger(__name__)

    def view_image(self, image_type, delay=0, transform=None):
        """
        displays the requested image on the screen
        """

        delay *= 1000  # convert milliseconds to seconds

        if image_type in ['og', 'original']:
            cv2.imshow("Original Image", self.og_image)
            cv2.waitKey(delay)
        elif image_type in ['tr', 'transform', 'transformed']:
            if self.transformed_image is not None:
                window_name = "Transformed Image: {0}".format(transform)
                cv2.imshow(window_name, self.transformed_image)
                cv2.waitKey(delay)

    def preprocess_image(self, resize_factor=None, view_transforms=True):
        """
        apply the transforms requested in the preprocess_pipe
        """

        # init
        target_image = None

        if len(self.preprocess_pipe) == 0 or self.preprocess_pipe is None:
            self.logger.info("No pre-processing will occur.")
            self.transformed_image = self.og_image
        else:
            for transform in self.preprocess_pipe:
                if self.transformed_image is None:
                    target_image = self.og_image
                elif self.transformed_image is not None:
                    target_image = self.transformed_image

                if target_image is None:
                    _exit_str = ("The target image value is None. Please confirm the file is being loaded correctly."
                                 "Traceback to self.og_image of OCRImage.")
                    self.logger.info(_exit_str)
                    sys.exit(_exit_str)

                # apply the transforms
                if transform == "gray":
                    # self.transformed_image = cv2.cvtColor(target_image, cv2.COLOR_BGR2GRAY)
                    self.transformed_image = self.og_image
                    if view_transforms is True:
                        self.view_image('og', 1)

                elif transform == "invert_black_white":
                    self.transformed_image = cv2.bitwise_not(src=target_image)

                elif transform == "opening":
                    self.transformed_image = cv2.morphologyEx(src=target_image,
                                                              op=cv2.MORPH_OPEN,
                                                              kernel=np.ones((5,5), np.uint8))

                elif transform == "resize":
                    if resize_factor is None:
                        self.logger.info("Defaulting resize_factor to 1.50 since None was set.")
                        resize_factor = 1.45
                    self.transformed_image = cv2.resize(src=target_image,
                                                        dsize=None,
                                                        fx=resize_factor,
                                                        fy=resize_factor,
                                                        interpolation=cv2.INTER_CUBIC)
                    self.logger.debug(type(self.transformed_image))

                elif transform == "custom_threshold":
                    self.transformed_image = cv2.threshold(src=target_image,
                                                           thresh=0,
                                                           maxval=255,
                                                           type=cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
                elif transform == "threshold_trunc":
                    self.transformed_image = cv2.threshold(src=target_image,
                                                           thresh=190,
                                                           maxval=255,
                                                           type=cv2.THRESH_TRUNC)[1]

                elif transform == "threshold_binary":
                    self.transformed_image = cv2.threshold(src=target_image,
                                                           thresh=205,
                                                           maxval=255,
                                                           type=cv2.THRESH_BINARY)[1]

                elif transform == "median_blur":
                    self.transformed_image = cv2.medianBlur(src=target_image,
                                                            ksize=3)  # median blurring to remove noise

                elif transform == "gaussian_blur":
                    self.transformed_image = cv2.GaussianBlur(src=target_image,
                                                              ksize=(3, 3),
                                                              sigmaX=1,
                                                              sigmaY=1)

                elif transform == "gaussian_adaptive":
                    self.transformed_image = cv2.adaptiveThreshold(src=target_image,
                                                                   maxValue=255,
                                                                   adaptiveMethod=cv2.ADAPTIVE_THRESH_GAUSSIAN_C,
                                                                   thresholdType=cv2.THRESH_BINARY,
                                                                   blockSize=99,
                                                                   C=5)

                else:
                    self.logger.info("Transform {0} does not exist".format(transform))

                if view_transforms is True:
                    self.view_image('tr', 3, transform)

    def apply_ocr(self, read_config: str = ''):
        """

        :param read_config:
        :return:
        """
        
        if defn.OCR_OUTPUT_TYPE in ['tsv', 'data']:
            # the "image_to_data" method outputs tsv
            ocr_results_tsv = pytesseract.image_to_data(image=self.transformed_image, config=read_config)
            self.logger.debug(ocr_results_tsv)
            self.logger.debug(io.StringIO(ocr_results_tsv))
            try:
                self.ocr_result_df = pd.read_csv(io.StringIO(ocr_results_tsv), sep='\t', header=0)
            except pd.errors.ParserError:
                self.ocr_result_df = pd.read_csv(io.StringIO(ocr_results_tsv), sep='\t', header=0, engine="python", error_bad_lines=False)
            self.total_row_count = len(self.ocr_result_df)

            self.logger.info("A total of {0} rows were returned by the OCR.".format(self.total_row_count))

        else:
            # load the image as a PIL/Pillow image, apply OCR, and then delete the temporary file
            ocr_results_string = pytesseract.image_to_string(image=self.transformed_image, config=read_config)

            text_ls = ocr_results_string.split('\n')
            text_ls = list(filter(None, text_ls))
            self.ocr_text_ls = list(filter(lambda x: (x not in [" ", "   "]), text_ls))
            self.total_row_count = len(self.ocr_text_ls)
            self.ocr_text_str = '\n'.join(self.ocr_text_ls).strip()

            self.logger.debug(self.ocr_text_str)
            self.logger.debug(self.ocr_text_ls)
