import numpy as np

from itemized_billing.utility import definitions as defn


def alter_endpoints_by_delta(x, top_delta=10, bottom_delta=10):
    """ adjust endpoint indices
    :param x: a list with two elements
    :param top_delta: how much whitespace is added to the top indices
    :param bottom_delta: how much whitespace is added to the bottom indices
    :return: x, modified
    """

    x[0] = x[0] - top_delta
    x[1] = x[1] + bottom_delta

    return x


def generate_file_signal(img_array, df):
    """
    given the image, generate the signal
    :param img_array: {numpy array}, the image broken down into pixels [R G B] 0-255
    :param df: {pandas dataframe} , where the results are being saved
    :return: df, modified
    """

    histogram_data = []

    print("Generating cutoffs for the document.")

    if defn.VERBOSE is True:
        # check min/max/mean/median of the array
        print('min: {} \nmax: {} \nmean: {}'.format((img_array.min()), img_array.max(), img_array.mean()))

    for i in range(len(img_array)):
        flat_list = [item for sublist in img_array[i] for item in sublist]
        histogram_data.append(sum(flat_list) / len(flat_list))

    df['average_row_value'] = histogram_data
    pixel_array = np.array(df['average_row_value']).reshape(-1, 1)
    df['normalized_average_row_value'] = (pixel_array - min(pixel_array)) / (max(pixel_array) - min(pixel_array))

    '''
    # graph the data
    series = Series(df['average_row_value'])
    series.plot()
    tmp_filename = self.generate_filepath(self.ddo.png_dir,
                                          self.ddo.document_id,
                                          "full",
                                          "png",
                                          "")

    if defn.SAVE_SIGNAL is True:
        pyplot.savefig(tmp_filename)

    if defn.SHOW_GRAPHS is True:
        pyplot.show()
    '''

    return df
