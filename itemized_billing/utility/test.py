import os


def convert_pdf_to_pngs(path, file):
    density_val = 400

    loc = "/".join([path, file]) + ".pdf"
    out = "/".join([path, file]) + ".png"

    print("Converting the pdf to pngs...")
    os.system("convert -density {0} {1} -quality 100 {2}".format(density_val, loc, out))
    print("Conversion complete.")


if __name__ == "__main__":
    path = "/home/joe/projects/_data_warehouse/srcing"
    file = "no_cover_pages_1"

    convert_pdf_to_pngs(path, file)
