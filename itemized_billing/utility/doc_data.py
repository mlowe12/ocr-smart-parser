import io
import logging
import os
import shutil
from typing import Optional, Sequence

import uuid

from PIL import Image

from itemized_billing.utility import definitions as defn
from itemized_billing.utility import utility


# Joe: is DocData a container(statically created) or an executor(runtime dominant)? It can't be both.
# Joe: we should decide on one in order to continue the effort towards developing unit tests
class DocumentData(object):
    # Joe: are you type checking because DocData is in fact an abstract data type container?
    # to-do: self.__dict__ debug print statement to cleanly indentify DocData object attributes  
    
    # type check
    target_file: Optional[str]
    document_tag: Optional[str]
    document_name: Optional[str]
    document_extension: Optional[str]
    document_id: str
    document_dir: str
    page_count: Optional[int]
    png_count: Optional[int]
    cp_count: Optional[int]
    ncp_count: Optional[int]
    png_dct: Optional[dict]
    png_dir: Optional[str]
    png_filenames: Optional[Sequence[str]]
    png_updated_filenames: Optional[list]
    document_config: Optional[dict]


    # delete this...
    # TODO @Mike add types for these
    # joe: TODO in progress
    api_file: Optional[str]
    # mime_type

    def __init__(self) -> None:
        self.logger = logging.getLogger(__name__)

        self.target_file = None  # the pdf we're working on, populated by get_available_document
        self.document_tag = None  # [tampa01]_sometext.pdf
        self.document_name = None  # tampa01_[sometext].pdf
        self.document_extension = None  # tampa01_sometext.[pdf]
        self.document_id = str(uuid.uuid1())  # a unique string assigned to the document
        self.document_dir = defn.GET_DOCUMENT_DIR  # a string such as "resources/test_docs""
        self.page_count = None

        self.png_count = None  # the number of pngs created by convert from the pdf
        self.cp_count = None  # the number of cover pages detected
        self.ncp_count = None  # the number of non-cover pages detected

        self.png_dct = None  # (key: png name) (value: BytesIO of png)
        self.png_dir = None  # the directory where the pngs are saved
        self.png_filenames = None  # the png filenames as created by convert
        self.png_updated_filenames = None  # the png filenames once they go through the page classifier

        # redundant variables-- type check is performed at controller level
        # delete these
        self.api_file = None
        self.mime_type = None

        self.document_config = None  # the configuration defined by the document_tag

    # mike: delete this... its redundant and useless for the docdata convert to png's
    def configure_api_process(self, filename, document) -> None:
        self.api_file = "/tmp/{0}".format(filename)
        # write in memory file to document in for convert command
        with open(self.api_file, 'wb') as f:
            f.write(document)

    # joe: this should be an abstracted method or a get/set property instead of a dynamic method    
    # joe: we should be able to build and support getting documents without comprimising the composition of the DocData object
    # ie: right now the class is both static and functional by paradigm--we should pick one to eliminate bugs
    def get_available_document(self, filename=None) -> None:
        """ this method returns a pdf file name that needs to be processed """

        if filename is None:
            self.target_file = utility.get_files_by_type(filepath=defn.BASE_DIR + "/" + self.document_dir, filetype="pdf")[0]
            self.logger.debug("files present %s: " % self.target_file)
        else:
            self.target_file = filename
            self.document_dir = "/tmp/{0}".format(filename)
            self.logger.debug(self.document_dir)

        # TODO Mike help with locking the file using ino table
        target_split = self.target_file.split("-")
        self.document_tag = target_split[0]
        self.document_name = target_split[1].split(".")[0]
        self.document_extension = target_split[1].split(".")[1]

    def convert_pdf_to_pngs(self, is_from_api: bool = False) -> None:
        
        # redudant paramter, just override object interface
        if is_from_api is True:
            self.png_dir = "/tmp/{0}-{1}".format(str(self.document_tag), str(self.document_id))
            pdf_fileloc = self.api_file
        else:
            self.png_dir = "/".join([defn.BASE_DIR, str(self.document_dir), str(self.document_tag) + "-" + str(self.document_id)])
            pdf_fileloc = "/".join([defn.BASE_DIR, self.document_dir, str(self.target_file)])

        os.mkdir(self.png_dir)
        os.chmod(self.png_dir, 0o777)

        self.logger.info("Converting the pdf to pngs...")
        os.system("convert -density {0} {1} -quality 100 {2}/%d.png".format(defn.CONVERT_DENSITY,
                                                                            pdf_fileloc,
                                                                            self.png_dir))
        # store the png filenames
        self.png_filenames = utility.get_files_by_type(self.png_dir, filetype="png", sort="REVERSE_FILENAME")

        # store the number of pngs that were created
        self.png_count = len(utility.get_files_by_type(self.png_dir, "png"))
        self.logger.info("pdf to png conversion complete. ({0} were created)".format(self.png_count))

        # load the pngs to memory
        png_ls: list = []
        self.png_dct = {}
        for fn in self.png_filenames:
            image = Image.open(self.png_dir + "/" + fn)

            with io.BytesIO() as output:
                image.save(output, format="PNG")
                contents = output.getvalue()

            png_ls.append(contents)

        for i, mem in enumerate(png_ls):
            self.png_dct[i] = mem

    def process_images(self) -> list:

        # init
        image_filepointers: list = []

        for key, value in self.png_dct.items():
            if key[0] == 'n':
                image_binary = io.BytesIO(value)
                image_filepointers.append(Image.open(image_binary))

        return image_filepointers

    # review method with joe during code review
    def to_string(self) -> str:
        return str((self.__dict__))

    def cleanup(self) -> None:
        """ delete the files that were created by the process """
        self.logger.info("Removing the temporary files that were created.")
        shutil.rmtree(self.png_dir)
        