import os
import logging
from typing import Sequence

import pandas as pd

from itemized_billing.ocr import parser, ocr


def process_ocr_documents(preprocessed_documents, ddo):
    # init
    logger = logging.getLogger(__name__)
    df_list = []

    for key, value in preprocessed_documents.items():
        tmp = ocr.OCRImage(value, preprocess_pipe=ddo.document_config['preprocess_pipe'], splice_flag=True)
        tmp.preprocess_image(resize_factor=1.45, view_transforms=False)
        tmp.apply_ocr('--psm 6')
        to_parser = tmp.ocr_result_df
        doc_parser = parser.Parser(to_parser)
        doc_parser.parse_document()
        df_list.append(doc_parser.parsed_dataframe)
    
    logger.debug(pd.concat(df_list).to_string(index=False))

    return pd.concat(df_list)


def get_files_by_type(filepath: str, filetype: str, sort: str = "DATE_ASC") -> Sequence[str]:
    """
    :param filepath:
    :param filetype: a string such as .png of png (supplying without the period is better)
    :param sort: str, default: "DATE_ASC" in what order should the files be returned?
    :return: list, a list of filenames as strings
    """

    ls: list = []

    for f in os.listdir(filepath):
        if f.split('.')[-1] == filetype:
            file_path_name = filepath + "/" + f
            ctime = os.stat(file_path_name)[-1]
            ls.append((f, ctime))

    if sort == "DATE_ASC":
        ls = sorted(ls, key=lambda tup: tup[1])
    elif sort == "DATE_DESC":
        ls = sorted(ls, key=lambda tup: tup[1], reverse=True)
    elif sort == "REVERSE_FILENAME":
        ls = sorted(ls, key=lambda s: s[::-1])

    filenames = [l[0] for l in ls]

    return filenames


def chunker(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]


if __name__ == "__main__":
    path = "/home/joe/projects/_data_warehouse/srcing"
    file = "no_cover_pages_1"
