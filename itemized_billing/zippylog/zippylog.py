import sys
from typing import Optional

import os
import json
import logging.config
import logging

PERFORMANCE_LEVEL_NUM = 25
logging.addLevelName(PERFORMANCE_LEVEL_NUM, "PERFORMANCE")


def add_performance_level(self, message, *args, **kws):
    if self.isEnabledFor(PERFORMANCE_LEVEL_NUM):
        self._log(PERFORMANCE_LEVEL_NUM, message, args, **kws)


def run(config_path_filename: str,
        log_dir: str,
        console_level: Optional[str] = None,
        env_key: str = 'LOG_CFG'):
    """
    setup logging configuration using a JSON file format
    :param config_path_filename: the path of the config file with the filename
    :param log_dir: where should the logs be stored?
    :param console_level: the level of logging that should be sent to the console
    :param env_key:
    :return: None, logging values are configured globally through this function
    """

    # init
    logging.Logger.performance = add_performance_level
    value = os.getenv(env_key, None)

    logging.captureWarnings(True)  # NOTE: warnings.warn('...') will go to the log NOT Raise UserWarning('...')
    uncaught_logger = logging.getLogger("uncaught_exceptions")

    # check if the user supplied console_level is valid, if it's not set it to 'INFO' which is valid
    supported_levels: list = list(logging._levelToName.values()) + ['PERFORMANCE']
    if console_level not in supported_levels:
        console_level = 'INFO'

    if value:
        config_path_filename = value

    if os.path.exists(config_path_filename):
        # case when `logger_config.json` is in the proper location
        with open(config_path_filename, 'rt') as f:
            config = json.load(f)

            config['handlers']['console']['level'] = console_level

            # update {info|debug|error}_file_handler log location if the directory exists
            try:
                config['handlers']['info_file_handler']['filename'] = log_dir + "/" + "info.log"
                config['handlers']['debug_file_handler']['filename'] = log_dir + "/" + "debug.log"
                config['handlers']['warning_file_handler']['filename'] = log_dir + "/" + "warning.log"
                config['handlers']['error_file_handler']['filename'] = log_dir + "/" + "error.log"
                config['handlers']['performance_file_handler']['filename'] = log_dir + "/" + "performance.log"
            except ValueError:
                error_msg_str = ("[CONFIRM] ??? will be used to hold the logs until the log directory is "
                                 "configured correctly. See README.md for more information.")
                print(error_msg_str)
                logging.debug(error_msg_str)

        try:
            logging.config.dictConfig(config)
        except ValueError:
            # when the log directory doesn't exist we need to create it first
            print("Creating the directory for logging at location {0} since it doesn't exist.".format(log_dir))
            os.mkdir(log_dir)
            logging.config.dictConfig(config)
    else:
        raise FileNotFoundError("logger_config.json was not located. please check the location in definitions.py \n"
                                "log_dir: {0}".format(config_path_filename))

    def exception_hook(exc_type, exc_value, exc_traceback):
        uncaught_logger.error("Uncaught exception", exc_info=(exc_type, exc_value, exc_traceback))

    sys.excepthook = exception_hook
