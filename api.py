import connexion
import threading
from flask import Flask, jsonify
from connexion.resolver import RestyResolver


if __name__ == "__main__":
    app = connexion.App(__name__, specification_dir='swagger/')
    app.add_api('app.yaml',resolver=RestyResolver('api'))
    app.run(port=5000)