OCR Intelligent Text Extraction Proof of Concept
---

## Unstructured OCR Data Wrangling

Application Process(es) focus on the extraction and structuring of text from medical bills in the following sections:

####Binary Page Content Classification processes:
      - Document component labeling is performed manually
      - Labeled data is used to train model on what IS and is NOT a cover page
      - Model output is a 1 or 0(cover page or not)
      - Cover page is discarded from process; data pages are sent to parser micro-service

![Regressor](docs/Regressor.png)

      
   
####Parser Configuration Builder
      - Process designed to send JSON structured configuration parameters to parser micro-service
      - Configuration Builder is structured based upon document shape determined by the Content Classier model
      - Acts a centralized store for all regonized document configurations

![](docs/JSON.png)


   
####Micro-service Stage Pipeline
      - Algorithmic deskewing of image and negative greyscale applied to image
      - Runs image OCR w/Tesseract and returns reponse in the form of a *.tsv file 
      - Staging Application for arranging the format, file, and configuration of response sent to Parser microservice 

![](docs/Microservice.png)

####Example of Algorithmic Deskewing
![Skew](docs/corrected-skew.png)
---
Binary Classification Example
---
Binary Classification Training Labels
---
#####Cover Page
![Cover Page](resources/training_data/cp-3-0.png)
#####Itemized Billing Data
![ Not Cover Page](resources/training_data/ncp-3-0.png)
#####Second Example of "non"-cover page data
![ Not Cover Page 2](resources/training_data/ncp-4-0.png)

---

## Image Parsing

OCR image extraction involves the feature analysis of both the image and text fields in the representation of a sum of sinusoids.
By observing the image as a finite discrete time signal.

#### Signal Profile 
![Signal](resources/test_docs/phi_free_1_1/signal_phi_free_1_1.png)
#### Signal Profile by document section 
![Signal](resources/test_docs/phi_free_1_1/signal_color.png)





The following generalizations can be made about the document.
These generalizations, can be applied to classifying areas of text via sinusoidal decomposition giving the following labels:
##### Header
![Header](resources/test_docs/phi_free_1_1/phi_free_1_1_split_vertical_whitespace_0.png)
---
##### Footer
![Footer](resources/test_docs/phi_free_1_1/phi_free_1_1_split_vertical_whitespace_3.png)
---
##### Body
![Body](resources/test_docs/phi_free_1_1/phi_free_1_1_split_vertical_whitespace_2.png)
---
